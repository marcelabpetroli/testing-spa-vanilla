import { Router } from "@vaadin/router";
import "./main.css";
import "./ui/post.ui";
import "./pages/home.page";
import "./pages/posts.page";
import "./components/header.component";
import "./components/posts.component";
import "./components/detail.component";

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/posts", component: "posts-page" },
  { path: "(.*)", redirect: "/" },
]);
