import { LitElement, html } from "lit";

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
      displayContent: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.displayContent = false;
  }

  getStyles() {
    return html` <style>
      .content {
        font-weight: 100;
        font-size: 1.6rem;
        margin-top: 1rem;
        padding: 1.5rem;
        border-top: 0.1rem solid var(-primary-color);
      }
    </style>`;
  }

  render() {
    const capitalize = (str) => {
      return str.charAt(0).toUpperCase() + str.slice(1);
    };

    return html`
      <div>
        ${this.displayContent
          ? html` <p @click="${this.handleDisplayContent}">
                ${capitalize(this.post.title)}
              </p>
              <p class="content">${capitalize(this.post.content)}</p>`
          : html`
              <p @click="${this.handleDisplayContent}">
                ${capitalize(this.post.title)}
              </p>
            `}
      </div>
      ${this.getStyles()}
    `;
  }

  handleDisplayContent() {
    this.displayContent = !this.displayContent;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("post-ui", PostUI);
