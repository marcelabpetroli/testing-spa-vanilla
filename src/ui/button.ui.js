import { LitElement, html } from "lit";

export class ButtonUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
      buttonContent: { type: String },
    };
  }

  getStyles() {
    return html` <style>
      button {
        --button-color: var(--link-color);
        --button-background: var(--primary-color);

        width: 10rem;
        height: 4rem;
        color: var(--button-color);
        background-color: var(--button-background);
        border: none;
        margin-left: 1rem;
        padding: 1rem 1rem;
        font-size: 1.8rem;
        border-radius: 2rem;
      }
      button:hover {
        --button-color: var(--primary-color);
        --button-background: var(--link-color);
        border: 0.1rem solid var(--primary-color);
      }
    </style>`;
  }

  render() {
    return html`
      <button>${this.buttonContent}</button>
      ${this.getStyles()}
    `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("button-ui", ButtonUI);
