import { LitElement, html } from "lit";

export class HeaderComponent extends LitElement {
  render() {
    const imgUrl = new URL("../assets/logo.jpeg", import.meta.url).href;
    const imgRocketUrl = new URL("../assets/rocket.png", import.meta.url).href;

    return html`
      <header class="header">
        <div class="header__container">
          <div class="image__container">
            <img
              class="image__container-img"
              src="${imgUrl}"
              alt="Genk-VII logo"
              title="Genk-VII logo"
            />
          </div>
          <h1>Never Stop Learning!</h1>
          <div class="icon__container">
            <img
              class="icon"
              src="${imgRocketUrl}"
              alt="Rocket icon"
              title="Rocket icon"
            />
          </div>
        </div>

        <nav class="nav">
          <a class="nav__link" id="navHome" href="/">Home</a>
          <a class="nav__link" id="navPost" href="/posts">Posts</a>
        </nav>
      </header>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("header-component", HeaderComponent);
