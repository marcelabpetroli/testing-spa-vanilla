import { LitElement, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { AddPostUseCase } from "../usecases/add-new-post.usecase";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";
import { UpdatePostUseCase } from "../usecases/update-post.usecase";
import "./../ui/post.ui";
import "./../ui/button.ui";

export class PostsComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
  }

  render() {
    return html`
      <section class="list post">
        <button-ui
          id="postsAddBtn"
          class="list__button"
          @click="${this.addPostMode}"
          buttonContent="Add"
        ></button-ui>
        <h1 class="list__title">Posts List</h1>
        <p class="list__results" id="listResults">
          You have ${this.posts?.length} posts
        </p>
        <ul class="list__items" id="listItems">
          ${this.posts?.map(
            (post) =>
              html`
                <li
                  class="list__item"
                  @click="${this.selectPost}"
                  id="${post.id}"
                >
                  <post-ui .post="${post}" id="post_${post.id}"></post-ui>
                </li>
              `
          )}
        </ul>
      </section>
    `;
  }

  selectPost(e) {
    const clickedPost = this.posts?.find(
      (post) => post.id === parseInt(e.currentTarget.id)
    );

    const prevSelectedPost = this.querySelector(".selected");
    prevSelectedPost?.classList.remove("selected");
    e.currentTarget.classList.add("selected");

    this.dispatchEvent(
      new CustomEvent("select:post", {
        bubbles: true,
        detail: {
          clickedPost: clickedPost,
        },
      })
    );
  }

  removeSelectedClass() {
    const selectPost = this.querySelector(".selected");
    selectPost.classList.remove("selected");
  }

  addPostMode() {
    this.removeSelectedClass();
    this.dispatchEvent(
      new CustomEvent("add:button", {
        bubbles: true,
      })
    );
  }

  async addNewPost(newPost) {
    const updatedPosts = await AddPostUseCase.execute(this.posts, newPost);
    this.posts = updatedPosts;
  }

  async deletePost(postId) {
    const updatedPosts = await DeletePostUseCase.execute(this.posts, postId);
    this.posts = updatedPosts;
  }

  async updatePost(updatedPost) {
    const updatedPosts = await UpdatePostUseCase.execute(
      this.posts,
      updatedPost
    );
    this.posts = updatedPosts;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("posts-component", PostsComponent);
