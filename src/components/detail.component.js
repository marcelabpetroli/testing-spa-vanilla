import { LitElement, html } from "lit";
import { Post } from "../model/post";
import "./../ui/button.ui";

export class DetailComponent extends LitElement {
  static get properties() {
    return {
      titleValue: { type: String },
      contentValue: { type: String },
      postId: { type: Number },
      isAddMode: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.titleValue = "";
    this.contentValue = "";
    this.postId = "";
    this.isAddMode = true;
  }

  render() {
    return html`
      <section class="form post">
        <h1 class="form__heading">Post Detail</h1>
        <form @submit="${this.submitForm}">
          <fieldset class="form__fieldset">
            <div class="form__group">
              <label class="form__label" for="postTitle">Title</label>
              <textarea
                class="form__textarea"
                rows="2"
                cols="20"
                type="text"
                id="postTitle"
                name="postTitle"
                placeholder="Title of your post"
                @change="${this.setTitleValue}"
                .value="${this.titleValue}"
              ></textarea>
            </div>

            <div class="form__group">
              <label class="form__label" for="postContent">Content</label>
              <textarea
                class="form__textarea"
                rows="5"
                cols="20"
                type="text"
                id="postContent"
                name="postContent"
                placeholder="Content of your post"
                @change="${this.setContentValue}"
                .value="${this.contentValue}"
              ></textarea>
            </div>

            <div class="form__group--buttons">
              ${this.isAddMode
                ? html`
                    <button-ui
                      class="form__button"
                      buttonContent="Cancel"
                      @click="${this.handleCancel}"
                    >
                    </button-ui>
                    <button-ui
                      id="addBtn"
                      class="form__button"
                      buttonContent="Add"
                      @click="${this.handleAdd}"
                    >
                    </button-ui>
                  `
                : html`<button-ui
                      id="cancelBtn"
                      class="form__button"
                      buttonContent="Cancel"
                      @click="${this.handleCancel}"
                    >
                    </button-ui>
                    <button-ui
                      id="updateBtn"
                      class="form__button"
                      buttonContent="Update"
                      @click="${this.handleUpdate}"
                    >
                    </button-ui>
                    <button-ui
                      id="deleteBtn"
                      class="form__button"
                      buttonContent="Delete"
                      @click="${this.handleDelete}"
                    >
                    </button-ui> `}
            </div>
          </fieldset>
        </form>
      </section>
    `;
  }

  // SETTERS //
  setTitleValue(e) {
    this.titleValue = e.target.value;
  }

  setContentValue(e) {
    this.contentValue = e.target.value;
  }

  setInputValues(post) {
    this.postId = post.id;
    this.titleValue = post.title;
    this.contentValue = post.content;
  }

  setDetailMode(value) {
    this.isAddMode = value;
    this.resetForm();
  }

  // HANDLERS //
  handleAdd() {
    const newPost = new Post({
      title: this.titleValue,
      content: this.contentValue,
    });

    const addPostEvent = new CustomEvent("add:post", {
      bubbles: true,
      detail: { newPost },
    });
    this.dispatchEvent(addPostEvent);
    this.resetForm();
  }

  handleDelete() {
    const deletePostEvent = new CustomEvent("delete:post", {
      bubbles: true,
      detail: { postId: this.postId },
    });
    this.dispatchEvent(deletePostEvent);
    this.resetForm();
  }

  handleUpdate() {
    const newPost = new Post({
      id: this.postId,
      title: this.titleValue,
      content: this.contentValue,
    });

    const updatePostEvent = new CustomEvent("update:post", {
      bubbles: true,
      detail: { updatedPost: newPost },
    });
    this.dispatchEvent(updatePostEvent);
    this.resetForm();
  }

  handleSelectedPostEvent(selectedPost) {
    this.isAddMode = false;
    this.resetForm();

    this.postId = selectedPost.id;
    this.titleValue = selectedPost.title;
    this.contentValue = selectedPost.content;
  }

  handleAddButtonEvent() {
    this.isAddMode = true;
    this.resetForm();
  }

  handleCancel() {
    this.isAddMode = true;

    const updatePostEvent = new CustomEvent("cancel:button", {
      bubbles: true,
    });
    this.dispatchEvent(updatePostEvent);
    this.resetForm();
  }

  // FORM //
  submitForm(e) {
    e.preventDefault();
  }

  resetForm() {
    this.titleValue = "";
    this.contentValue = "";
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("detail-component", DetailComponent);
