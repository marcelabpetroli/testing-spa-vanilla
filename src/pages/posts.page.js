export class PostsPage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
      posts-page {
        width: 100%;
        height: 100%;
        display: grid;
        grid-template-columns: 1fr 2fr;
        grid-gap: 1rem;
      }
      #posts {
        margin-left: 3rem;
      }
      #detail {
       margin-right: 3rem;
      }
    </style>
    `;
  }
  connectedCallback() {
    this.innerHTML = `
        <posts-component id="posts"></posts-component>
        <detail-component id="detail"></detail-component>
        ${this.getStyles()}
        `;

    const postsComponent = this.querySelector("#posts");
    const detailComponent = this.querySelector("#detail");

    this.addEventListener("add:post", (e) => {
      postsComponent.addNewPost(e.detail.newPost);
    });
    this.addEventListener("delete:post", (e) => {
      postsComponent.deletePost(e.detail.postId);
    });
    this.addEventListener("update:post", (e) => {
      postsComponent.updatePost(e.detail.updatedPost);
    });

    this.addEventListener("select:post", (e) => {
      detailComponent.handleSelectedPostEvent(e.detail.clickedPost);
    });

    this.addEventListener("add:button", (e) => {
      detailComponent.handleAddButtonEvent();
    });
    this.addEventListener("cancel:button", (e) => {
      postsComponent.removeSelectedClass();
    });
  }
}

customElements.define("posts-page", PostsPage);
