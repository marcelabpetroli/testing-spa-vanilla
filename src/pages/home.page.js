export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
      home-page {
        height: 100%;
      }
      section {
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        
      }
      h1 {
        font-size: 2.4rem;
      }
    </style>
    `;
  }
  connectedCallback() {
    this.innerHTML = `
    <section>
    <h1>Feeling Creative Today?</h1>
    </section>
    ${this.getStyles()}
    `;
  }
}

customElements.define("home-page", HomePage);
