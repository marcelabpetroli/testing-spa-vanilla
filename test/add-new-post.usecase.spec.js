import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostUseCase } from "../src/usecases/add-new-post.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Add post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });
  it("should add new post", async () => {
    const POSTS = [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 2,
        title: "qui est esse",
        body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
      {
        userId: 1,
        id: 3,
        title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
        body: "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut",
      },
    ];
    const newPost = {
      title: "My New Title",
      content: "This is the new content of my post",
    };

    PostsRepository.mockImplementation(() => {
      return {
        addNewPost: () => {
          return {
            id: 101,
            title: newPost.title,
            body: newPost.content,
            userId: 1,
          };
        },
      };
    });

    const updatedPosts = await AddPostUseCase.execute(POSTS, newPost);

    expect(updatedPosts.length).toBe(POSTS.length + 1);
    expect(updatedPosts[0].title).toBe(newPost.title);
    expect(updatedPosts[0].content).toBe(newPost.content);
  });
});
