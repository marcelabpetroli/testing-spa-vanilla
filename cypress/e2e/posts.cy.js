/// <reference types="Cypress" />

describe("template spec", () => {
  it("should open home page", () => {
    cy.visit("/");
  });

  it("should open posts page", () => {
    cy.get("#navPost").click();
  });

  beforeEach(() => {
    cy.visit("/");
    cy.get("#navPost").click();
  });

  it("should create a new post", () => {
    cy.get("#postTitle").type("Heard in The Office");
    cy.get("#postContent").type("'He put my stuff in jello again'. - Dwight");
    cy.get("#addBtn").click();
    cy.get("#listItems").children().should("have.length", 101);
    cy.get("#post_101 > div > p").contains("Heard in The Office");
    cy.get("#post_101 > div > p").click();
    cy.get("#post_101 > div > .content").contains(
      "'He put my stuff in jello again'. - Dwight"
    );
  });

  it("should delete a post", () => {
    cy.get("#post_1").click();
    cy.get("#deleteBtn > button").click();
    cy.get("#listItems").children().should("have.length", 100);
  });

  it("should update the title and content of an existing post", () => {
    cy.get("#post_2").click();
    cy.get("#postTitle").clear();
    cy.get("#postTitle").type("My New Title");
    cy.get("#postContent").clear();
    cy.get("#postContent").type("New Content");
    cy.get("#updateBtn").click();
    cy.get("#post_2 > div > p").contains("My New Title");
    cy.get("#post_2 > div > .content").contains("New Content");
  });

  it("should reset to empty the content of the form", () => {
    cy.get("#post_3").click();
    cy.get("#cancelBtn").click();
    cy.get("#postTitle").should("have.value", "");
    cy.get("#postContent").should("have.value", "");
    cy.get("#postTitle").should(
      "have.attr",
      "placeholder",
      "Title of your post"
    );
    cy.get("#postContent").should(
      "have.attr",
      "placeholder",
      "Content of your post"
    );
  });

  it("should set the form to add mode", () => {
    cy.get("#post_2").click();
    cy.get("#postsAddBtn").click();
    cy.get("#postTitle").should("have.value", "");
    cy.get("#postContent").should("have.value", "");
    cy.get('[buttoncontent="Cancel"] > button').should("exist");
    cy.get("#addBtn > button").should("exist");
    cy.get("#cancelBtn").should("not.exist");
    cy.get("#updateBtn").should("not.exist");
  });

  it("should go back to home page", () => {
    cy.get("#navHome").click();
  });
});
